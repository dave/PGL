
import numpy as np
from PGL.main.bezierpatch import BezierPatch


p = BezierPatch(4, 4)
m = np.meshgrid(np.linspace(0, 2, 4), np.linspace(0, 1, 4))
p.CPs = np.ones((4, 4, 3))
p.CPs[:, :, 0] = m[0]
p.CPs[:, :, 1] = m[1]
p.CPs[1:-1, 0, 0] = -.5
p.CPs[1, 1, 2] = 0.
# zero at corners
p.CPs[0, 0, 2] = 0.
p.CPs[-1, 0, 2] = 0.
p.CPs[0, -1, 2] = 0.
p.CPs[-1, -1, 2] = 0.

p.update()
