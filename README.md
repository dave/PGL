
# PGL - Parametric Geometry Library

PGL is a Python based tool for creating surface geometries using simple parametric inputs.
PGL is tailored towards wind turbine related geometries, but its base classes can be used for any purpose.
The package contains a series of classes for generating geometric primitives such as airfoils, blade surfaces, nacelles, towers etc,
that can be run as scripts with input files.

PGL also has interfaces to DTU Wind Energy's hyperbolic mesh generators HypGrid2D and HypGrid3D,
which allow for smooth mesh generation around 2D and 3D geometries.

## Installation and requirements

PGL installs as a standard Python distribution and requires Python 2.7.x, numpy and scipy >= 0.14.
To use the plotting functions in PGL you need to have Mayavi installed, which is practically impossible to install unless you use a packaged version.
It it therefore highly recommended to install Python using the [Anaconda distribution](https://store.continuum.io/cshop/anaconda/) and install Mayavi via this.

To install PGL simply run

    $ python setup.py develop

## Documentation

PGL is documented using Sphinx, and you can build the docs by navigating to the ``docs`` directory and issueing the command:

    $ make html

To view the docs open _build/html/index.html in your a browser.

## Examples

A number of examples of how to use PGL is located in ``PGL/examples``.
