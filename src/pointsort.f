      subroutine pointsort(x,y,ni,xt,yt,slist)
      implicit none
      real(kind=8),dimension(ni)::x,y,xt,yt,cp,cf
      real(kind=8),dimension(2)::v1,v2
      real(kind=8)::xs,ys,len,l1,l2,atest,atestmax,lminn,factor
      real(kind=8),dimension(5)::lmin
      integer,dimension(ni)::slist
      logical,dimension(ni)::pcheck
      integer::ni,i,j,k,sorted,illast
      integer,dimension(5)::ilast
      integer,dimension(1)::iloc

      factor=2.d0

      pcheck=.true.
      slist=0
c------first point
c     print*,' Give start point for sorting : '
c     read(*,*)ilast(1)
      ilast(1)=1
      if(ilast(1).lt.0)then
        iloc=maxloc(x)
        ilast(1)=iloc(1)
      endif
      slist(1)=ilast(1)
      xs=x(ilast(1));ys=y(ilast(1))
      pcheck(ilast(1))=.false.
c------second point
      lmin(1)=.25d0
      do i=1,ni
        if(pcheck(i))then
          len=sqrt((x(i)-xs)**2
     &            +(y(i)-ys)**2)
          if(len.lt.lmin(1))then
              lmin(1)=len
              ilast(1)=i
          endif
        endif
      enddo
      slist(2)=ilast(1)
      pcheck(ilast(1))=.false.
      xs=x(ilast(1))
      ys=y(ilast(1))
c-------remaining point
      do j=3,ni
        lmin=1.d0;ilast=-1
        v2(1)=x(slist(j-1))-x(slist(j-2))
        v2(2)=y(slist(j-1))-y(slist(j-2))
        l2=sqrt(v2(1)**2+v2(2)**2)
c--------- find the five nearest points
        do i=1,ni
          if(pcheck(i))then
            len=sqrt((x(i)-xs)**2
     &              +(y(i)-ys)**2)
             if(len.lt.lmin(1))then
               do k=5,2,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(1)=len;ilast(1)=i
             elseif(len.lt.lmin(2))then
               do k=5,3,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(2)=len;ilast(2)=i
             elseif(len.lt.lmin(3))then
               do k=5,4,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(3)=len;ilast(3)=i
             elseif(len.lt.lmin(4))then
               lmin(5)=lmin(4);ilast(5)=ilast(4)
               lmin(4)=len;ilast(4)=i
             elseif(len.lt.lmin(5))then
               lmin(5)=len;ilast(5)=i
             endif
           endif
         enddo
         atestmax=-1.d0;lminn=1.d0
         do k=1,4
           if(ilast(k).gt.0)then
             v1(1)=x(ilast(k))-xs
             v1(2)=y(ilast(k))-ys
             l1=sqrt(v1(1)**2+v1(2)**2)
             atest=(v1(1)*v2(1)+v1(2)*v2(2))
     &            /(l1*l2)
             if(k.eq.1.and.atest.gt..4d0)then
               illast=ilast(k)
               goto 100
             endif
             if(atest.gt.atestmax*1.5.and.l1.lt.1.05*lminn)then
               atestmax=atest;lminn=l1
               illast=ilast(k)
             endif
           endif
         enddo
  100   slist(j)=illast
        pcheck(slist(j))=.false.
        xs=x(slist(j))
        ys=y(slist(j))
      enddo
      ilast=slist(1)

      do j=1,ni
        xt(j)=x(slist(j))
        yt(j)=y(slist(j))
      enddo
      return
      end

      subroutine reorder(x,y,ni,xt,yt,slist)
      implicit none
      integer ni,i,imax,ii
      integer,dimension(ni)::slist
      integer,dimension(ni)::slistt
      real(kind=8),dimension(ni)::x,y,xt,yt
      real(kind=8)dx,ym,area,xmax

c------determine orientation of line by computing integral
      area=0.d0
      do i=2,ni-1
        dx=x(i+1)-x(i-1) 
        ym=.5*(y(i+1)+y(i)) 
        area=area+dx*ym
      enddo
c     write(*,*)' area ',area/abs(area)
c------make orientation clockwise
c     if(area.lt.0.d0)then
c     do i=1,ni
c       xt(i)=x(ni+1-i)
c       yt(i)=y(ni+1-i)
c     enddo
c     x=xt;y=yt
c     endif
c-------make first point trailing edge point
      xmax=-10
      do i=1,ni
        if(x(i).ge.xmax)then
          imax=i
          xmax=x(i)
        endif
      enddo
      ii=0
      do i=imax,ni
        ii=ii+1
        xt(ii)=x(i)
        yt(ii)=y(i)
        slist(ii)=i
      enddo
      do i=1,imax-1
        ii=ii+1
        xt(ii)=x(i)
        yt(ii)=y(i)
        slist(ii)=i
      enddo
      x=xt;y=yt
      if(area.lt.0.d0)then
      do i=1,ni
        xt(i)=x(ni+1-i)
        yt(i)=y(ni+1-i)
        slistt(i)=slist(ni+1-i)
      enddo
      x=xt;y=yt
      slist=slistt
      endif
c     x=xt;y=yt;cp=cpt;cf=cft
      return
      end

