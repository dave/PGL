
import numpy as np

from PGL.main.curve import Curve
from PGL.components.coonsblade import CoonsBladeSection, CoonsBlade
from PGL.components.airfoil import AirfoilShape, BlendAirfoilShapes

# c0 = np.loadtxt('data/ffaw3241.dat')
# c0 = np.vstack((c0.T, np.zeros(c0.shape[0]))).T
# c0 = Curve(c0)
# c0.redistribute(s=np.linspace(0, 1, 257))
# c1 = np.loadtxt('data/ffaw3360.dat')
# c1 = np.vstack((c1.T, np.ones(c1.shape[0]))).T
# c1 = Curve(c1)
# c1.redistribute(s=np.linspace(0, 1, 257))
#
# d = CoonsBladeSection(c0.points, c1.points)
# d.fWs = [0.01, 0.01]
# d.interpolant='linear'
# d.update()

interpolator = BlendAirfoilShapes()
interpolator.ni = 257
interpolator.spline = 'pchip'
interpolator.blend_var = [0.241, 0.301, 0.36, 1.0]
for f in ['data/ffaw3241.dat',
          'data/ffaw3301.dat',
          'data/ffaw3360.dat',
          'data/cylinder.dat']:

    interpolator.airfoil_list.append(np.loadtxt(f))
interpolator.initialize()

pf = np.loadtxt('data/DTU_10MW_RWT_blade_axis_prebend.dat')

s = [0., 0.05, 0.2, 0.3, 0.4, 0.6, 0.8,0.97, 1.]
rthick = np.interp(s, pf[:,2]/86.366, pf[:,7]/100.)
chord = np.interp(s, pf[:,2]/86.366, pf[:,6]/100.)
twist = np.interp(s, pf[:,2]/86.366, pf[:,5])
p_le = np.interp(s, pf[:,2]/86.366, pf[:,8])

chord[[0, 1]] = 4.5/86.366
# chord[-1] = 0.001

bl = CoonsBlade()

bl.np = 4
bl.close_te_flag = True
bl.chord_ni = 257

dp = ['z', 'z', -1, -1, -1, -1, -1, -1, -1]
fWs = [0.5] * 9
fWs[-2] = 0.25
fWs[-1] = 0.01

for i, s in enumerate(s):
    bl.add_cross_section(interpolator(rthick[i]), pos=np.array([0, 0, s]),
                                     rot=np.array([0, 0, twist[i]]),
                                     chord=chord[i],
                                     p_le=p_le[i],
                                     dp=dp[i], fWs=fWs[i])


# bl._compute_cross_sections()
# bl.compute_sections()
# bl.set_C1()
# bl.update_sections()
# bl.apply_transforms()
bl.update()
