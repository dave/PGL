
Blade Mesher
============

The :class:`PGL.components.blademesher.BladeMesher` generates a 3D surface mesh around a rotor, combining the classes for :ref:`blade surface <sec-loftedbladesurface>`, :ref:`root <sec-coonsbladeroot>` or :ref:`nacelle <sec-coonsnacelle>`, and :ref:`blade tip <sec-coonsbladetip>`.


.. toctree::
   :maxdepth: 2

   blademesher_components


There are two examples that generate a mesh around a rotor; one with a simple cylindrical root connection, and another with a full spinner and nacelle geoemtry.

BladeMesher Simple Root Example
---------------------------------

In this example, we will generate a mesh around the three bladed DTU 10MW RWT with a simple cylindrical root connection.

The example is located in ``PGL/examples/blademesher_example.py``.

.. literalinclude:: ../PGL/examples/blademesher_example.py


BladeMesher With Nacelle Example
----------------------------------

To build the rotor mesh including a spinner and nacelle, you need to supply a few extra inputs.
The example in ``PGL/examples/blademesher_nacelle_example.py`` shows how to add a spinner/nacelle, replacing the simple cylindrical root from the above example.

.. literalinclude:: ../PGL/examples/blademesher_nacelle_example.py

The nacelle curve is in this example generated manually from a composite curve consisting of a Bezier curve and a straight line.
Note that since the nacelle and spinner will be generated as a rotationally symmetric shape, the curve has to describe only the half the geometry, and should be open ended.

.. figure:: /figures/nacellecurve.*
    :align: center

Instead of generating the nacelle shape manually, you can load in the shape from a file.

The resulting mesh should look like this:

.. figure:: /figures/rotor_with_nacelle.*
    :align: center


BladeMesher One Bladed Example
------------------------------

In this example, we will generate a mesh around a single blade which requires the use of the :class:`PGL.components.bladecap.RootCap` class.

The example is located in ``PGL/examples/blademesher_1b_example.py``.

.. literalinclude:: ../PGL/examples/blademesher_1b_example.py
