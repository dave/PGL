
import numpy as np
import copy
from scipy.optimize import minimize
from scipy.interpolate import pchip, Akima1DInterpolator, interp1d

from PGL.main.geom_tools import curvature
from PGL.main.curve import Curve
from PGL.main.bezier import FitBezier
from PGL.main.naturalcubicspline import NaturalCubicSpline


class AirfoilShape(Curve):
    """
    Base class for airfoil shapes.

    The class automatically computes the LE and TE
    and can redistribute the points smoothly along the surface.
    Points along the surface need to be defined starting at the
    TE pressure side ending at the TE suction side.
    """

    def initialize(self, points, nd=None):

        if nd is not None and nd > points.shape[1]:
            points = np.vstack((points.T, np.zeros(points.shape[0]))).T

        super(AirfoilShape, self).initialize(points)
        self.computeLETE()

    def _build_splines(self):

        self._splines = []

        for j in range(self.points.shape[1]):
            self._splines.append(self._splcls(self.s, self.points[:, j]))

    def computeLETE(self):
        """
        computes the leading and trailing edge of the airfoil.

        TE is computed as the mid-point between lower and upper TE points
        LE is computed as the point with maximum distance from the TE.
        """

        def _sdist(s):

            x = self._splines[0](s).real
            y = self._splines[1](s).real
            return -((x - self.TE[0].real)**2 + (y - self.TE[1].real)**2)**0.5

        self.TE = np.zeros(self.nd)
        for i in range(self.nd):
            self.TE[i] = np.average(self.points[[0, -1], i])

        res = minimize(_sdist, (0.5), method='SLSQP', bounds=[(0, 1)], tol=1.e-16)
        self.sLE = res['x'][0]
        self.iLE = np.where(np.abs(self.s - self.sLE) == np.abs(self.s - self.sLE).min())[0]
        xLE = self._splines[0](self.sLE)
        yLE = self._splines[1](self.sLE)
        self.LE = np.zeros(self.nd)
        self.LE[0] = xLE
        self.LE[1] = yLE
        if self.nd > 2:
            self.LE[2] = self._splines[2](self.sLE)
        self.chord = np.linalg.norm(self.LE-self.TE)
        self.curvLE = NaturalCubicSpline(self.s, curvature(self.points/self.chord))(self.sLE)

    def leading_edge_dist(self, ni):
        """ function that returns a suitable cell size based on airfoil LE curvature """

        min_ds1 = 1. / ni * 0.1
        max_ds1 = 1. / ni * 0.5

        ds1 = max((min_ds1 - max_ds1) / 30. * abs(self.curvLE) + max_ds1, min_ds1)

        return ds1

    def redistribute(self, ni, even=False, dist=None, dLE=False, dTE=-1.,
                     close_te=0, linear=False):
        """
        redistribute the points on the airfoil using fusedwind.lib.distfunc

        Parameters
        ----------
        ni : int
            total number of points on airfoil
        even : bool
            flag for getting an even distribution of points
        dist : list
            optional list of control points with the form
            [[s0, ds0, n0], [s1, ds1, n1], ... [s<n>, ds<n>, n<n>]]
            where\n
            s<n> is the normalized curve fraction at each control point,\n
            ds<n> is the normalized cell size at each control point,\n
            n<n> is the cell count at each control point.
        dLE : float
            optional float specifying cell size at LE.
            If not specified, a suitable leading edge cell size is
            automatically calculated based on the local curvature
        dTE : float
            optional trailing edge cell size. If set to -1 the cell size will increase
            from the LE to TE according to the tanh distribution function used
            in distfunc
        close_te: int
            close trailing edge with line segment consisting of n points.
        """

        if close_te > 0:
            if close_te % 2 == 0:
                close_te += 1

            # construct the trailing edge segments
            lte_seg = self.TE.copy()
            lte_seg = np.append(lte_seg, self.points[0, :]).reshape(2, self.nd)
            lte_seg = Curve(points=lte_seg, spline='linear')
            lte_seg.redistribute(s=np.linspace(0, 1, close_te/2 + 1))
            ute_seg = self.points[-1, :]
            ute_seg = np.append(ute_seg, self.TE).reshape(2, self.nd)
            ute_seg = Curve(points=ute_seg, spline='linear')
            ute_seg.redistribute(s=np.linspace(0, 1, close_te/2 + 1))

            main_seg = Curve(points=self.points, spline=self.spline_type, compute_dp=False)
            minTE = lte_seg.ds[-1] / main_seg.smax
            main_ni = ni - close_te + 1
            if dLE:
                dist_LE = dLE
            else:
                dist_LE = self.leading_edge_dist(main_ni)
            dist = [[0., minTE, 1],
                    [self.sLE, dist_LE, main_ni / 2],
                    [1., minTE, main_ni]]
            main_seg.redistribute(dist=dist, linear=linear)
            points = lte_seg.points.copy()
            points = np.append(points, main_seg.points[1:], axis=0)
            points = np.append(points, ute_seg.points[1:], axis=0)
            # self.initialize(points)
            self.points = points
            self.ni = ni
        else:
            if dist == None:
                if even:
                    dist = [[0, 1./np.float(ni-1), 1], [self.sLE, 1./np.float(ni-1), int(ni*self.sLE)], [1, 1./np.float(ni-1), ni]]
                elif dLE:
                    dist = [[0., dTE, 1], [self.sLE, dLE, ni / 2], [1., dTE, ni]]
                else:
                    dist = [[0., dTE, 1], [self.sLE, self.leading_edge_dist(ni), ni / 2], [1., dTE, ni]]

            super(AirfoilShape, self).redistribute(dist, linear=linear)

        return self

    def redistribute_chordwise(self, dist):
        """
        redistribute the airfoil according to a chordwise distribution
        """

        # self.redistribute(self.ni, even=True)
        iLE = np.argmin(self.points[:,0])
        ni = dist.shape[0]
        dist = np.asarray(dist)
        points = np.zeros((dist.shape[0] * 2 - 1, self.points.shape[1]))

        # interpolate pressure side coordinates
        yps = NaturalCubicSpline(self.points[:iLE+1, 0][::-1],
                                 self.points[:iLE+1, 1][::-1])
        ps = yps(dist)
        # interpolate suction side coordinates
        yss = NaturalCubicSpline(self.points[iLE:,0],
                                 self.points[iLE:,1])
        ss = yss(dist)
        points[:ni-1, 0] = dist[::-1][:-1]
        points[ni-1:, 0] = dist
        points[:, 1] = np.append(ps[::-1][:-1], ss, axis=0)
        return AirfoilShape(points)

    def s_to_11(self, s):
        """
        Transform the s coordinates from AirfoilShape format:

        * s=0 at TE pressure side (lower surface)
        * s=1 at TE suction side (upper surface)

        to the s coordinates from the input definition:

        * s=0 at LE
        * s=1 at TE suction side (upper surface)
        * s=-1 at TE pressure side (lower surface)
        """

        if s > self.sLE:
            return (s-self.sLE) / (1.0-self.sLE)
        else:
            return -1.0 + s/self.sLE

    def s_to_01(self, s):
        """
        Transform the s coordinates from the input definition:

        * s=0 at LE
        * s=1 at TE suction side (upper surface)
        * s=-1 at TE pressure side (lower surface)

        to the backend defintion compatible with AirfoilShape():

        * s=0 at TE pressure side (lower surface)
        * s=1 at TE suction side (upper surface)
        """
        if s >= 0.0:
            return s*(1.0-self.sLE) + self.sLE
        else:
            return (1.0+s)*self.sLE

    def gurneyflap(self, gf_height, gf_length_factor):
        """add a Gurney flap shaped using a tanh function"""

        if gf_height == 0.: return
        # if the length is not specified it is set to 3 x gf_height
        gf_length = gf_length_factor * gf_height

        # identify starting point of the gf along the chord
        x_gf = 1. - gf_length
        id1 = (np.abs(x_gf - self.points[0:self.ni / 2, 0])).argmin() + 1
        s = np.linspace(x_gf, self.points[0, 0], 100)
        smax = s[-1] - s[0]
        h = np.zeros(100)
        for i in range(100):
            h[i] = (min(.90 * gf_height, gf_height*(-np.tanh((s[i] - s[0])/smax*3)+1.)))/0.90
        h = h[::-1]
        self.gfs = s
        self.gfh = h

        # add the gf shape to the airfoil
        points = self.points.copy()
        for i in range(0,id1):
            points[i,1] = points[i,1] - np.interp(points[i, 0], s, h)

        return AirfoilShape(points)

    def open_trailing_edge(self, t):
        """
        add thickness to airfoil
        """

        t0 = np.abs(self.points[-1, 1] - self.points[0, 1])
        dt = (t - t0) / 2.
        # linearly add thickness from LE to TE
        iLE = np.argmin(self.points[:,0])
        xLE = self.points[iLE, 0]
        tlin = np.array([np.linspace(xLE, self.TE[0], 100),
                        np.linspace(0., dt, 100)]).T

        tspline = NaturalCubicSpline(tlin[:, 0], tlin[:, 1])

        ys = tspline(self.points[iLE:, 0]) + self.points[iLE:, 1]
        yp = -tspline(self.points[:iLE, 0][::-1])[::-1] + self.points[:iLE, 1]

        self.points[iLE:, 1] = ys
        self.points[:iLE, 1] = yp
        self.initialize(self.points)

    def interp_x(self, x, side):
        """
        interpolate s(x) for lower or upper side
        """
        if self.LE[0] < self.TE[0]:
            iLE = np.argmin(self.points[:, 0])
            iTEl = np.argmax(self.points[:iLE, 0])
            iTEu = np.argmax(self.points[iLE:, 0]) + iLE
            if x < self.points[iLE, 0]:
                return self.points[iLE, 1]
            elif x > self.TE[0]:
                return 0.
        else:
            iLE = np.argmax(self.points[:, 0])
            iTEl = np.argmin(self.points[:iLE, 0])
            iTEu = np.argmin(self.points[iLE:, 0]) + iLE
            if x > self.points[iLE, 0]:
                return self.sLE
            elif x < self.TE[0]:
                return 0.
        if side == 'lower':
            # interpolate pressure side coordinates
            s = self.points[iTEl:iLE, 0]
            if s[0] > s[-1]:
                s = s[::-1]
                y = self.s[iTEl:iLE][::-1]
            else:
                y = self.s[iTEl:iLE]
            spl = NaturalCubicSpline(s, y)
            return spl(x)

        if side == 'upper':
            # interpolate pressure side coordinates
            s = self.points[iLE:iTEu, 0]
            if s[0] > s[-1]:
                s = s[::-1]
                y = self.s[iLE:iTEu][::-1]
            else:
                y = self.s[iLE:iTEu]
            spl = NaturalCubicSpline(s, y)
            return spl(x)

class BlendAirfoilShapes(object):
    """
    Blend input airfoil shape family based on a user defined scalar.

    The blended airfoil shape is interpolated using a cubic interpolator
    of the airfoil shapes.
    Three interpolators are implemented:\n
    ``scipy.interpolate.pchip``: has some unappealing characteristics at the bounds\n
    ``fusedwind.lib.cubicspline``: can overshoot significantly with large spacing in
    thickness\n
    ``scipy.interpolate.Akima1DInterpolator``: good compromise, overshoots less
    than a natural cubic spline\n

    The default spline is scipy.interpolate.Akima1DInterpolator.

    Parameters
    ----------
    ni: int
        number of redistributed points on airfoils

    airfoil_list: list
        List of normalized airfoils with size ((ni, 2)).

    blend_var: list
        weight factors for each airfoil in the list, only relevant if tc is not specified.

    spline: str
        spline type, either ('pchip', 'cubic', 'akima')

    allow_extrapolation: bool
        the splines allow for limited extrapolation, set to True if you feel lucky
    """


    def __init__(self, **kwargs):

        self.airfoil_list = []
        self.ni = 600
        self.blend_var = None
        self.spline = 'pchip'
        self.allow_extrapolation = False

        for k,v in kwargs.iteritems():
            if hasattr(self,k):
                setattr(self,k,v)

    def initialize(self):

        if self.spline == 'linear':
            self._spline = interp1d
        if self.spline == 'pchip':
            self._spline = pchip
        elif self.spline == 'cubic':
            self._spline = NaturalCubicSpline
        elif self.spline == 'akima':
            self._spline = Akima1DInterpolator

        self.blend_var = np.asarray(self.blend_var)

        afs = []
        for af in self.airfoil_list:
            a = AirfoilShape(af)
            aa = a.redistribute(ni=self.ni, even=True)
            afs.append(aa.points)
        self.airfoil_list = afs

        self.nj = len(self.airfoil_list)
        self.nk = 3

        self.x = np.zeros([self.ni,self.nj,self.nk])
        for j, points in enumerate(self.airfoil_list):
            self.x[:,j,:2] = points[: , :2]
            self.x[:,j,2] = self.blend_var[j]

        self.f = [[],[]]
        for k in range(2):
            for i in range(self.ni):
                self.f[k].append(self._spline(self.x[0, :, 2], self.x[i, :, k]))

    def __call__(self, tc):
        """
        interpolate airfoil family at a given thickness

        Parameters
        ----------
        tc : float
            The relative thickness of the wanted airfoil.

        Returns
        -------
        airfoil: array
            interpolated airfoil shape of size ((ni, 2))
        """

        # check for out of bounds
        if not self.allow_extrapolation:
            if tc < np.min(self.blend_var): tc = self.blend_var.min()
            if tc > np.max(self.blend_var): tc = self.blend_var.max()

        points = np.zeros((self.ni, 2))
        for k in range(2):
            for i in range(self.ni):
                points[i, k] = self.f[k][i](tc)

        return points


class BezierAirfoilShape(object):
    """
    Class for fitting a composite Bezier curve
    to an airfoil shape.
    """

    def __init__(self):

        self.spline_CPs = np.array([])
        self.CPu = np.array([])
        self.CPl = np.array([])
        self.CPle = 0.
        self.CPte = 0.

        self.afIn = AirfoilShape()

        self.afOut = AirfoilShape()

        self.ni = 0
        self.fix_x = True
        self.symmLE = True
        self.symmTE = True
        self.fd_form = 'fd'

    def update(self):

        self.spline_eval()
        if self.ni > 0:
            self.afOut.redistribute(ni=self.ni)

    def fit(self):


        iLE = np.argmin(self.afIn.points[:, 0])

        # lower side
        curve_in = Curve(points=self.afIn.points[:iLE + 1, :][::-1])
        CPs = np.array(self.spline_CPs)
        for n in range(1, curve_in.points.shape[1]):
            CPs = np.append(CPs, np.zeros(self.spline_CPs.shape[0]))

        CPs = CPs.reshape(curve_in.points.shape[1], CPs.shape[0] / curve_in.points.shape[1]).T
        CPs[1:-1, 1] = -0.1

        constraints = np.ones((self.spline_CPs.shape[0], self.afIn.points.shape[1]))
        constraints[1, 0] = 0

        fit = FitBezier(curve_in, CPs, constraints)
        fit.fix_x = self.fix_x
        fit.execute()
        self.fit0 = copy.deepcopy(fit)

        self.spline_ps = copy.deepcopy(fit.curve_out)

        # upper side
        curve_in = Curve(points=self.afIn.points[iLE:, :])
        CPs = np.array(self.spline_CPs)
        for n in range(1, curve_in.points.shape[1]):
            CPs = np.append(CPs, np.zeros(self.spline_CPs.shape[0]))

        CPs = CPs.reshape(curve_in.points.shape[1], CPs.shape[0] / curve_in.points.shape[1]).T
        CPs[1:-1, 1] = 0.1
        constraints = np.ones((self.spline_CPs.shape[0], self.afIn.points.shape[1]))
        constraints[1, 0] = 0.
        fit = FitBezier(curve_in, CPs, constraints)
        fit.fix_x = self.fix_x
        fit.execute()
        self.fit1 = copy.deepcopy(fit)
        self.spline_ss = copy.deepcopy(fit.curve_out)

        self.CPl = self.spline_ps.CPs.copy()
        self.CPu = self.spline_ss.CPs.copy()
        self.nCPs = self.CPl.shape[0] + self.CPu.shape[0]
        self.CPle = 0.5 * (self.CPu[1, 1] - self.CPl[1, 1])
        self.CPte = 0.5 * (self.CPu[-1, 1] - self.CPl[-1, 1])
        if self.fd_form == 'complex_step':
            self.CPl = np.array(self.CPl, dtype=np.complex128)
            self.CPu = np.array(self.CPu, dtype=np.complex128)
            self.CPle = np.complex128(self.CPle)
            self.CPte = np.complex128(self.CPte)
        self.spline_eval()

    def spline_eval(self):
        """
        compute the Bezier spline shape given control points in CPs.
        CPs has the shape ((2 * spline_CPs.shape[0], 2)).
        """

        nd = self.CPl.shape[1]
        self.spline_ps.CPs = self.CPl
        if self.symmLE:
            self.spline_ps.CPs[1, 1] = -self.CPle
        if self.symmTE:
            self.spline_ps.CPs[-1, 1] = -self.CPte
        self.spline_ps.CPs[0] = np.zeros(nd)
        self.spline_ps.update()
        self.spline_ss.CPs = self.CPu
        if self.symmLE:
            self.spline_ss.CPs[1, 1] = self.CPle
        if self.symmTE:
            self.spline_ss.CPs[-1, 1] = self.CPte
        self.spline_ss.CPs[0] = np.zeros(nd)
        self.spline_ss.update()
        points = self.spline_ps.points[::-1]
        points = np.append(points, self.spline_ss.points[1:])
        points = points.reshape(points.shape[0]/nd, nd)
        self.afOut.initialize(points)


def compute_airfoil_props(points, ndiv, x_sec):
    """
    method for computing geometric properties of
    an airfoil.

    parameters
    ----------
    points: array
        airfoil coordinates defined from TE pressure side
        to LE to TE suction side.
    ndiv: int
        number of chordwise points on resplined airfoil
    x_sec: array
        chordwise locations at which to compute
        thicknesses which can more easily be used
        for constraints.

    returns
    -------
    props: dict
        dictionary of properties containing

        | x: (array) chordwise coordinates from LE to TE

        | ys: (array) y-coordinates of suction side

        | yp: (array) y-coordinates of pressure side

        | t: (array) thickness distribution

        | mean: (array) mean line of airfoil

        | mean_angle: (array) angle of mean line

        | curv_s: (array) curvature of suction side

        | curv_p: (array) curvature of pressure side

        | tmax: (float) maximimum thickness

        | tmin: (float) minimum thickness

        | tmax_s: (float) maximum thickness of suction side

        | tmax_p: (float) maximum thickness of pressure side

        | x_tmax: (float) chordwise position of maximimum thickness

        | x_tmax_s: (float) chordwise position of suction side maximimum thickness

        | x_tmax_p: (float) chordwise position of pressure side maximimum thickness

        | t_skew: (float) chordwise offset between position of suction and pressure side maximum thickness

        | mean_max: (float) minimum value of mean line

        | mean_min: (float) maximum value of mean line
    """

    props = {}

    iLE = np.argmin(points[:, 0])
    TE = np.max(points[:, 0])
    LE = np.min(points[:, 0])

    # equally spaced point distribution along chord
    props['x'] = np.linspace(LE, TE, ndiv, dtype=points.dtype)

    # interpolate pressure side coordinates
    yps = NaturalCubicSpline(points[:iLE+1, 0][::-1],
                             points[:iLE+1, 1][::-1])
    props['yp'] = np.asarray(yps(props['x']))
    # interpolate suction side coordinates
    yss = NaturalCubicSpline(points[iLE:, 0],
                             points[iLE:, 1])
    props['ys'] = np.asarray(yss(props['x']))

    # airfoil thickness distribution
    props['t'] = props['ys'] - props['yp']
    props['tmin'] = props['t'].min()

    # chordwise position of tmax
    tspline = NaturalCubicSpline(props['x'], -props['t'])
    res = minimize(tspline, (.3), method='SLSQP', tol=1.e-16, bounds=[(0,1)])
    props['x_tmax'] = res['x'][0]
    props['tmax'] = -tspline(props['x_tmax'])
    props['t_sec'] = -tspline(x_sec)
    # chordwise position of tmax @ pressure side
    # x = yps()
    res = minimize(yps, (.3), method='SLSQP', tol=1.e-16, bounds=[(0,1)])
    props['x_tmax_p'] = res['x'][0]
    props['tmax_p'] = -yps(props['x_tmax_p'])

    # chordwise position of tmax @ suction side
    tspline = NaturalCubicSpline(points[iLE:, 0],
                                -points[iLE:, 1])
    res = minimize(tspline, (.3), method='SLSQP', tol=1.e-16, bounds=[(0,1)])
    props['x_tmax_s'] = res['x'][0]
    props['tmax_s'] = -yss(props['x_tmax_s'])

    # airfoil skewness
    props['t_skew'] = props['x_tmax_s'] - props['x_tmax_p']

    # airfoil mean line
    props['mean'] = 0.5 * (props['yp'] + props['ys'])
    props['mean_max'] = props['mean'].max()
    props['mean_min'] = props['mean'].min()
    # airfoil mean line angle
    grad = np.gradient(np.array([props['x'], props['mean']]).T)[0]
    dydx = grad[:, 1] / grad[:, 0]
    props['mean_angle'] = np.zeros(ndiv, dtype=props['x'].dtype)
    if props['x'].dtype == np.complex128:
        for i in range(dydx.shape[0]):
            props['mean_angle'][i] = (np.math.atan(dydx[i].real) + dydx[i].imag*1j) / \
                                     (1. + dydx[i].real**2) * 180./np.pi
    else:
        for i in range(dydx.shape[0]):
            props['mean_angle'][i] = np.math.atan(dydx[i])*180./np.pi

    # suction and pressure side curvature
    curv = curvature(points)
    curv_p = NaturalCubicSpline(points[:iLE+1, 0][::-1], curv[:iLE+1][::-1])
    curv_s = NaturalCubicSpline(points[iLE:, 0], curv[iLE:])

    props['curv_p'] = curv_p(props['x'])
    props['curv_s'] = curv_s(props['x'])

    return props
