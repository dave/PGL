
import numpy as np

from PGL.main.planform import RedistributedPlanform, read_blade_planform
from PGL.components.loftedblade import LoftedBladeSurface
from PGL.components.bladeroot import CoonsBladeRoot
from PGL.components.bladecap import RootCap
from PGL.components.bladetip import CoonsBladeTip
from PGL.components.nacelle import CoonsNacelle
from PGL.main.domain import Domain, Block
from PGL.main.curve import Curve


class BladeMesher(object):
    """
    Generates a CFD ready structured grid blade surface mesh

    Parameters
    ----------
    pf: dict
        optional dictionary containing planform. If not supplied, planform_filename is required. Keys:

        |  s: normalized running length of blade
        |  x: x-coordinates of blade axis
        |  y: y-coordinates of blade axis
        |  z: z-coordinates of blade axis
        |  rot_x: x-rotation of blade axis
        |  rot_y: y-rotation of blade axis
        |  rot_z: z-rotation of blade axis
        |  chord: chord distribution
        |  rthick: relative thickness distribution
        |  p_le: pitch axis aft leading edge distribution
    planform_filename: str
        optional path to planform file. If not supplied, pf is required.
    base_airfoils_path: list
        list of string paths to airfoils
    interp_type: str
        airfoil interpolation blending variable: rthick or span
    blend_var: array
        airfoil interpolation blending factors, which will typically
        be the relative thicknesses of the airfoils in base_airfoils.
    ni_span: int
        number of points in the spanwise direction (including root)
    ni_chord: int
        number of points in the chordwise direction
    chord_nte: int
        number of points on trailing edge
    redistribute_flag: bool
        flag for switching on chordwise redistribution of points along the span,
        defaults to True if close_te = True.
    dist_LE: array
        2D array containing LE cell size as function of normalized span.
        If empty, LE cell size will be set according to LE curvature.
    minTE: float
        minimum trailing edge thickness.
    surface_spline: str
        spline type used to interpolate airfoil family
    gf_heights: array
        array containing s, gf_height, gf_length factor
    tip_fLE1: float
        Leading edge connector control in spanwise direction.
        pointy tip 0 <= fLE1 => 1 square tip.
    tip_fLE2: float
        Leading edge connector control in chordwise direction.
        pointy tip 0 <= fLE1 => 1 square tip.
    tip_fTE1: float
        Trailing edge connector control in spanwise direction.
        pointy tip 0 <= fLE1 => 1 square tip.
    tip_fTE2: float
        Trailing edge connector control in chordwise direction.
        pointy tip 0 <= fLE1 => 1 square tip.
    tip_fM1: float
        Control of connector from mid-surface to tip.
        straight line 0 <= fM1 => orthogonal to starting point.
    tip_fM2: float
        Control of connector from mid-surface to tip.
        straight line 0 <= fM2 => orthogonal to end point.
    tip_fM3: float
        Controls thickness of tip.
        Zero thickness 0 <= fM3 => 1 same thick as tip airfoil.
    tip_dist_cLE: float
        Cell size at tip leading edge starting point.
    tip_dist_cTE: float
        Cell size at tip trailing edge starting point.
    tip_dist_tip: float
        Cell size of LE and TE connectors at tip.
    tip_dist_mid0: float
        Cell size of mid chord connector start.
    tip_dist_mid1: float
        Cell size of mid chord connector at tip.
    tip_c0_angle: float
        Angle of connector from mid chord to LE/TE
    tip_nj_LE: int
        Index along mid-airfoil connector used as starting point for tip connector
    root_type: str
        options are 'cylinder' or 'nacelle', 'cap' (for 1 bladed turbines)
    hub_length: float
        length of the hub, extending from spinner end to nacelle start
    nacelle_curve: array
        2D nacelle shape curve oriented in x-y with blade root center
        at x=0 and spinner tip at -x
    nacelle_shape_file: str
        file containing 2D cross sectional shape of nacelle
        (only needed if nacelle_curve is not provided)
    ds_nacelle: float
        cell size at nacelle start
    nb_nacelle: int
        number of blocks in the flow direction on nacelle surface
    base_nv: int
        number of points on hub in spanwise direction (ni on blade will be ni_root - base_nv)
    cap_Fcap: float
        size of four block patch as a fraction of the root
        diameter, range 0 < Fcap < 1.
    cap_Fblend: float
        factor controlling shape of four block patch.

        | Fblend => 0. takes the shape of tip_con,
        | Fblend => 1. takes the shape of a rectangle.
    cap_direction: float
        Blade direction along z-axis: 1 positive z, -1 negative z

    Returns
    -------
    domain: object
        PGL.main.domain.Domain object containing the surface mesh
    """

    def __init__(self, **kwargs):
        super(BladeMesher, self).__init__()

        self.s_tip_start = 0.99
        self.s_tip = 0.995
        self.s_root_start = 0.0
        self.s_root_end = 0.05
        self.ds_root_start = 0.008
        self.ds_root_end = 0.005
        self.ds_tip_start = 0.001
        self.ds_tip = 0.00005
        self.ni_span = 129
        self.ni_root = 8
        self.ni_tip = 11
        self.n_refined_tip = 50
        self.dist = np.array([])

        self.refine_tip = False
        self.dist_LE = np.array([])

        self.tip_fLE1 = .5
        self.tip_fLE2 = .5
        self.tip_fTE1 = .5
        self.tip_fTE2 = .5
        self.tip_fM1 = 1.
        self.tip_fM2 = 1.
        self.tip_fM3 = .3
        self.tip_dist_cLE = 0.0002
        self.tip_dist_cTE = 0.0002
        self.tip_dist_tip = 0.0002
        self.tip_dist_mid0 = 0.0002
        self.tip_dist_mid1 = 0.00004
        self.tip_c0_angle = 30.
        self.tip_nj_LE = 20
        self.Ptip = np.array([])

        self.nblades = 3
        self.root_diameter = 0.
        self.root_type = 'cylinder'
        self.nb_nacelle = 1

        self.pf = {}
        self.planform_filename = ''
        self.base_airfoils = []
        self.base_airfoils_path = []
        self.blend_var = np.array([])
        self.ni_chord = 257
        self.ni_span = 129
        self.chord_nte = 11
        self.redistribute_flag = False
        self.minTE = 0.
        self.interp_type = 'rthick'
        self.surface_spline = 'pchip'
        self.user_surface_file = ''
        self.user_surface = np.array([])
        self.gf_heights = np.array([])
        self.rot_order = np.array([2,1,0])
        self.cone_angle = 0.
        self.shear_sweep = False

        self.connectors = []
        self.domain = Domain()

        for k, w, in kwargs.iteritems():
            if hasattr(self, k):
                setattr(self, k, w)

    def update(self):

        # redistribute the planform
        if len(self.planform_filename) > 0:
            self.pf = read_blade_planform(self.planform_filename)
        self.pf_spline = RedistributedPlanform(**self.__dict__)
        if self.user_surface_file or self.user_surface.shape[0] > 0:
            self.pf_spline.only_s = True
        self.pf_spline.update()
        self.pf = self.pf_spline.pf_out
        if not self.user_surface_file and not self.user_surface.shape[0] > 0:
            ax = Curve(points=np.array([self.pf['x'], self.pf['y'], self.pf['z']]).T)
            ax.rotate_x(self.cone_angle)
            self.pf['x'] = ax.points[:, 0]
            self.pf['y'] = ax.points[:, 1]
            self.pf['z'] = ax.points[:, 2]

        itip_start = self.ni_span - self.ni_tip

        # generate main section
        if len(self.base_airfoils_path) > 0:
            self.base_airfoils = []
        for name in self.base_airfoils_path:
            self.base_airfoils.append(np.loadtxt(name))
        self.main_section = LoftedBladeSurface(**self.__dict__)
        self.main_section.update()

        # generate root
        if self.ni_root > 0:
            if self.root_type == 'cylinder':
                self.root = CoonsBladeRoot(**self.__dict__)
            elif self.root_type == 'nacelle':
                self.root = CoonsNacelle(**self.__dict__)
            elif self.root_type == 'cap':
                self.root = RootCap(**self.__dict__)

            if self.root_type in ['cylinder', 'nacelle']:
                self.root.tip_con = self.main_section.surface[:, self.ni_root-1, :]
            elif self.root_type == 'cap':
                self.root.tip_con = self.main_section.surface[:, 0, :]
            if self.root_type == 'cylinder' and self.root_diameter == 0.:
                try:
                    self.root.root_diameter = self.pf['chord'][0]
                except:
                    raise RuntimeError('You need to specify the parameter `root_diameter`'
                                       'since theres no planform provided')
            self.root.update()

        # generate tip
        self.tip = CoonsBladeTip(**self.__dict__)
        self.tip.main_section = self.main_section.surface
        self.tip.ibase = itip_start
        axis = np.array([self.pf['x'],
                         self.pf['y'],
                         self.pf['z']]).T
        self.tip.axis = Curve(points=axis)
        if self.Ptip.shape[0] == 0:
            try:
                self.tip.Ptip = np.array([self.pf['x'][-1],
                                          self.pf['y'][-1],
                                          self.pf['z'][-1]])
            except:
                raise RuntimeError('You need to specity the parameter `Ptip`'
                                   'since theres no planform provided')
        self.tip.update()

        # assemble mesh and join blocks
        x = self.main_section.surface
        self.domain = Domain()
        if self.root_type in ['cylinder', 'nacelle']:
            ni_root = self.ni_root
        elif self.root_type == 'cap':
            ni_root = 0
        self.domain.add_blocks(Block(x[:, np.maximum(0, ni_root-1):itip_start+1, 0],
                                     x[:, np.maximum(0, ni_root-1):itip_start+1, 1],
                                     x[:, np.maximum(0, ni_root-1):itip_start+1, 2], name='main_section'))
        if self.ni_root > 0:
            self.domain.add_domain(self.root.domain)
            self.domain.join_blocks('root', 'main_section', newname='main_section')

        self.domain.add_domain(self.tip.domain)
        self.domain.join_blocks('tip-base', 'main_section', newname='main_section')
        self.domain.add_group('blade1', self.domain.blocks.keys())

    def add_dist_point(self, s, ds, index):
        """
        Add distribution points to distfunc

        Parameters
        ----------
        s : float
            Curve fraction, where 0 is at the root, and 1 at the tip.
        ds : float
            Normalized distance between points. Total curve length is 1. When
            set to -1 distfunc will figure something out.
        index : int
            Force index number on the added point.
        """

        self.dist = np.asarray(self.dist)

        try:
            if s in self.dist[:, 0]:
                return
        except:
            pass
        try:
            self.dist = np.append(self.dist, np.array([[s, ds, index]]), axis=0)
        except:
            self.dist = np.array([[s, ds, index]])

        self.dist = self.dist[np.argsort(self.dist[:,0]),:]
