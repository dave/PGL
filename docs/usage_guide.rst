
Usage Guide
===========

PGL consists of a series of classes, methods and example scripts.
An easy way to use and interact with PGL is to run the examples in an interactive iPython terminal,
which will allow you to inspect the objects and their parameters, and easily plot curves and the meshes.

You will get the most out of PGL if you also install Mayavi.
If you installed PGL using the Anaconda package, you can install it from a terminal with the command:


.. code-block:: bash

   $ conda install mayavi


You can simply start iPython with no options and run examples and call the plot functions of the Domain class and the mesh should appear in a separate window. For example

.. code-block:: bash

   $ cd PGL/examples
   $ ipython
   In [1]: run vg_example.py
   In [2]: v.domain.plot_surface_grid(edges=True)


To view and edit the source code, the `Atom <https://atom.io>`_ editor is a good candidate, since you can easily search for files in a directory tree and display multiple files in tabs.
If you prefer Spyder, you may be out of luck, since the iPython terminal in Spyder does not seem to play well with Mayavi.
However, the choice of editor is an almost religiuos subject, so I will leave it at that.
