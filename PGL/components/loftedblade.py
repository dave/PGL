
import numpy as np
from scipy.interpolate import pchip
import time

from PGL.main.domain import Domain, Block
from PGL.main.curve import Curve
from PGL.main.geom_tools import RotMat, dotXC
from PGL.components.airfoil import AirfoilShape, BlendAirfoilShapes


class LoftedBladeSurface(object):
    """
    Generates a lofted blade surface based on an airfoil family and
    a planform definition.

    If the axis defined in the planform is out of plane,
    this is taken into account in the positioning and rotation of
    the cross sections.

    The spanwise distribution is dictated by the planform points
    distribution.

    Parameters
    ----------
    pf: dict
        dictionary containing planform:
        s: normalized running length of blade
        x: x-coordinates of blade axis
        y: y-coordinates of blade axis
        z: z-coordinates of blade axis
        rot_x: x-rotation of blade axis
        rot_y: y-rotation of blade axis
        rot_z: z-rotation of blade axis
        chord: chord distribution
        rthick: relative thickness distribution
        p_le: pitch axis aft leading edge distribution
    base_airfoils: list of arrays
        airfoil family
    user_surface: array
        optional lofted surface which will override the use of the planform
        definition and only use carry out a spanwise redistribution
    user_surface_file: str
        path to previously processed surface saved as a flattened array.
    user_surface_shape: tuple
        shape of user surface i.e. (257,  150, 3)
    interp_type: str
        airfoil interpolation blending variable: rthick or span
    blend_var: array
        airfoil interpolation blending factors, which will typically
        be the relative thicknesses of the airfoils in base_airfoils.
    ni_chord: int
        number of points in the chordwise direction
    redistribute_flag: bool
        flag for switching on chordwise redistribution of points along the span,
        defaults to True if close_te = True.
    dist_LE: array
        2D array containing LE cell size as function of normalized span.
        If empty, LE cell size will be set according to LE curvature.
    minTE: float
        minimum trailing edge thickness.
    surface_spline: str
        spline type used to interpolate airfoil family
    chord_nte: int
        number of points on trailing edge
    gf_heights: array
        array containing s, gf_height, gf_length factor
    """
    def __init__(self, **kwargs):

        # planform
        self.pf = {'s': np.array([]),
                   'x': np.array([]),
                   'y': np.array([]),
                   'z': np.array([]),
                   'rot_x': np.array([]),
                   'rot_y': np.array([]),
                   'rot_z': np.array([]),
                   'chord': np.array([]),
                   'rthick': np.array([]),
                   'p_le': np.array([])
                    }

        self.base_airfoils = []
        self.blend_var = np.array([])
        self.user_surface = np.array([])
        self.user_surface_file = ''
        self.user_surface_shape = ()
        self.ni_chord = 257
        self.chord_nte = 11
        self.redistribute_flag = False
        self.x_chordwise = np.array([])
        self.minTE = 0.
        self.interp_type = 'rthick'
        self.surface_spline = 'pchip'
        self.dist_LE = np.array([])
        self.gf_heights = np.array([])
        self.shear_sweep = False

        self.rot_order = np.array([2,1,0])

        for k, w, in kwargs.iteritems():
            if hasattr(self, k):
                setattr(self, k, w)

        self.domain = Domain()
        self.surface = np.array([])

    def update(self):
        """
        generate the surface
        """

        t0 = time.time()

        if self.user_surface_file:
            print 'reading user surface from file', self.user_surface_file
            x = np.loadtxt(self.user_surface_file).reshape(self.user_surface_shape)
            self.surface = x
            self.domain = Domain()
            self.domain.add_blocks(Block(x[:, :, 0], x[:, :, 1], x[:, :, 2]))

        elif self.user_surface.shape[0] > 0:
            self.redistribute_surface()

        else:
            self.initialize_interpolator()
            self.build_blade()

        print 'lofted surface done ...', time.time() - t0

    def initialize_interpolator(self):

        self.interpolator = BlendAirfoilShapes()
        self.interpolator.ni = self.ni_chord
        self.interpolator.spline = self.surface_spline
        self.interpolator.blend_var = self.blend_var
        self.interpolator.airfoil_list = self.base_airfoils
        self.interpolator.initialize()

    def build_blade(self):

        self.s = self.pf['s']
        self.x = self.pf['x']
        self.y = self.pf['y']
        self.z = self.pf['z']
        self.rot_x = self.pf['rot_x']
        self.rot_y = self.pf['rot_y']
        self.rot_z = self.pf['rot_z']
        self.chord = self.pf['chord']
        self.rthick = self.pf['rthick']
        self.p_le = self.pf['p_le']



        self.ni_span = self.s.shape[0]
        x = np.zeros((self.ni_chord, self.ni_span, 3))

        if self.gf_heights.shape[0] > 0:
            self.gf_height = pchip(self.gf_heights[:, 0], self.gf_heights[:, 1])
            self.gf_length_factor = pchip(self.gf_heights[:, 0], self.gf_heights[:, 2])

        self.LE = np.zeros((self.ni_span, 3))
        self.TE = np.zeros((self.ni_span, 3))
        for i in range(self.ni_span):

            s = self.s[i]
            pos_x = self.x[i]
            pos_y = self.y[i]
            pos_z = self.z[i]
            chord = self.chord[i]
            p_le = self.p_le[i]

            # generate the blended airfoil shape
            if self.interp_type == 'rthick':
                rthick = self.rthick[i]
                points = self.interpolator(rthick)
            else:
                points = self.interpolator(s)

            points = self._redistribute(points, pos_z, i)

            points *= chord
            points = self._open_trailing_edge(points, pos_z)
            points[:, 0] -= chord * p_le

            # x-coordinate needs to be inverted for clockwise rotating blades
            x[:, i, :] = (np.array([-points[:,0], points[:,1], x.shape[0] * [pos_z]]).T)

        # save blade without sweep and prebend
        x_norm = x.copy()

        # add translation and rotation
        x[:, :, 0] += self.x
        x[:, :, 1] += self.y
        x = self._rotate(x)

        self.surfnorot = x_norm
        self.surface = x
        self.domain = Domain()
        self.domain.add_blocks(Block(x[:, :, 0], x[:, :, 1], x[:, :, 2]))

    def redistribute_surface(self):

        self.s = self.pf['s']

        self.interpolator = BlendAirfoilShapes()
        self.interpolator.ni = self.ni_chord
        self.interpolator.spline = self.surface_spline
        self.interpolator.blend_var = self.user_surface[0, :, 2]
        self.interpolator.airfoil_list = [self.user_surface[:, i, :] for i in range(self.user_surface.shape[1])]
        self.interpolator.initialize()

        self.ni_span = self.s.shape[0]
        x = np.zeros((self.ni_chord, self.ni_span, 3))
        self.LE = np.zeros((self.ni_span, 3))
        self.TE = np.zeros((self.ni_span, 3))
        for i in range(self.ni_span):
            points = self.interpolator(self.s[i])
            points = self._open_trailing_edge(points, self.s[i])
            points = self._redistribute(points, self.s[i], i)

            # x-coordinate needs to be inverted for clockwise rotating blades
            x[:, i, :] = np.array([points[:,0], points[:,1], x.shape[0] * [self.s[i]]]).T

        self.surface = x
        self.domain = Domain()
        self.domain.add_blocks(Block(x[:, :, 0], x[:, :, 1], x[:, :, 2]))

    def _rotate(self, x):
        """
        rotate blade sections accounting for twist and main axis orientation

        if the main axis includes a winglet, the blade sections will be
        rotated accordingly. ensure that an adequate point distribution is
        used in this case to avoid sections colliding in the winglet junction!
        """

        main_axis = Curve(points=np.array([self.x, self.y, self.z]).T)

        rot_normals = np.zeros((3,3))
        x_rot = np.zeros(x.shape)

        shear = 1.
        if self.shear_sweep: shear = 0.

        for i in range(x.shape[1]):
            points = x[:, i, :]
            rot_center = main_axis.points[i]
            # rotation angles read from file
            angles = np.array([self.rot_x[i],
                               self.rot_y[i],
                               self.rot_z[i]]) * np.pi / 180.

            # compute rotation angles of main_axis
            t = main_axis.dp[i]
            rot = np.zeros(3)
            rot[0] = -np.arctan(t[1]/(t[2]+1.e-20))
            v = np.array([t[2], t[1]])
            vt = np.dot(v, v)**0.5
            a = np.maximum(np.minimum(1., t[0]/vt), -1.)
            rot[1] = np.arcsin(a)
            angles[0] += rot[0]
            angles[1] += rot[1]
            angles[1] *= shear
            # compute x-y-z normal vectors of rotation
            n_y = np.cross(t, [1,0,0])
            n_y = n_y / np.linalg.norm(n_y)
            rot_normals[0, :] = np.array([1,0,0])
            rot_normals[1, :] = n_y
            rot_normals[2, :] = t

            # compute final rotation matrix
            rotation_matrix = np.matrix([[1.,0.,0.],[0.,1.,0.],[0.,0.,1.]])
            for n, ii in enumerate(self.rot_order):
                mat = np.matrix(RotMat(rot_normals[ii], angles[ii]))
                rotation_matrix = mat * rotation_matrix

            # apply rotation
            x_rot[:, i, :] = dotXC(rotation_matrix, points, rot_center)

        return x_rot

    def _redistribute(self, points, pos_z, i):

        if self.redistribute_flag == False:
            return points

        airfoil = AirfoilShape(points=points, spline='cubic')
        try:
            dist_LE = np.interp(pos_z, self.dist_LE[:, 0], self.dist_LE[:, 1])
        except:
            dist_LE = None
        # pass airfoil to user defined routine to allow for additional configuration
        airfoil = self._set_airfoil(airfoil, pos_z)
        if self.x_chordwise.shape[0] > 0:
            airfoil = airfoil.redistribute_chordwise(self.x_chordwise)
        else:
            dTE = airfoil.smax / self.ni_chord / 10.
            airfoil = airfoil.redistribute(ni=self.ni_chord, dLE=dist_LE, dTE=dTE, close_te=self.chord_nte)
        self.LE[i] = np.array([airfoil.LE[0], airfoil.LE[1], pos_z])
        self.TE[i] = np.array([airfoil.TE[0], airfoil.TE[1], pos_z])
        return airfoil.points

    def _open_trailing_edge(self, points, pos_z):
        """
        Ensure that airfoil training edge thickness > minTE
        """
        if self.minTE == 0.:
            return points

        af = AirfoilShape(points=points)
        t = np.abs(af.points[-1,1] - af.points[0, 1])
        if t < self.minTE:
            af.open_trailing_edge(self.minTE)

        return af.points

    def _set_airfoil(self, airfoil, pos_z):

        if hasattr(self, 'gf_height'):
            height = self.gf_height(pos_z)
            length_factor = self.gf_length_factor(pos_z)
            print 'gf', pos_z, height, length_factor
            if height > 0.:
                airfoil = airfoil.gurneyflap(height, length_factor)

        return airfoil
